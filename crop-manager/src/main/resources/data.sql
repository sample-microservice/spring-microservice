INSERT INTO CROPS(id, name, scientific_name) VALUES (1, 'potato', 'sci potato');
INSERT INTO CROPS(id, name, scientific_name) VALUES (2, 'carrot', 'sci carrot');
INSERT INTO CROPS(id, name, scientific_name) VALUES (3, 'sweet potato', 'sci sweet potato');
INSERT INTO CROPS(id, name, scientific_name) VALUES (4, 'tomato', 'sci tomato');