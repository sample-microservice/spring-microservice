package me.potato.sample.msa.cropmanager.crop;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
public class CropController {

    private final CropRepository crops;

    public CropController(CropRepository crops) {
        this.crops = crops;
    }

    @GetMapping("/")
    public List<Crop> getAllCrops() {
        return crops.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity getCrop(@PathVariable("id") Long id) {
        log.info("get a "+id);
        Optional<Crop> crop = crops.findById(id);
        if (crop.isPresent())
            return ResponseEntity.ok().body(crop);
        else
            return ResponseEntity.noContent().build();
    }
}
