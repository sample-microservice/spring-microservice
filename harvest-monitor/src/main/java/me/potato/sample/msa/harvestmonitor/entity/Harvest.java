package me.potato.sample.msa.harvestmonitor.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import me.potato.sample.msa.harvestmonitor.entity.Crop;

@Getter
@Setter
@ToString
public class Harvest {
    private Long id;

    private Crop crop;

}
