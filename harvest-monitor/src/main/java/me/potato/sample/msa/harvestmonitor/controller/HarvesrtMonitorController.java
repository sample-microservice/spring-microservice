package me.potato.sample.msa.harvestmonitor.controller;

import lombok.extern.slf4j.Slf4j;
import me.potato.sample.msa.harvestmonitor.entity.Crop;
import me.potato.sample.msa.harvestmonitor.entity.Harvest;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@Slf4j
@RestController
public class HarvesrtMonitorController {

    private final RestTemplate restTemplate;

    private final Environment env;

    public HarvesrtMonitorController(RestTemplate restTemplate, Environment env) {
        this.restTemplate = restTemplate;
        this.env = env;
    }

    @GetMapping("/{id}")
    public ResponseEntity getCrop(@PathVariable("id") Long id) {
        log.info("harvest "+id);

        ResponseEntity<Crop> crop = restTemplate.getForEntity("http://crop-manager/" + id, Crop.class);

        if (crop.getStatusCode().is2xxSuccessful()) {

            Harvest harvest = new Harvest();
            harvest.setId(id);
            harvest.setCrop(crop.getBody());

            return ResponseEntity.ok().body(harvest);
        }else {
            return ResponseEntity.noContent().build();
        }
    }

    @RequestMapping("/admin")
    public String management(){
        return "This is Admin area";
    }
}
