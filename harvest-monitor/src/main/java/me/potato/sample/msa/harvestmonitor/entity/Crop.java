package me.potato.sample.msa.harvestmonitor.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Crop {
    private Long id;

    private String name;

    private String scientificName;

}
