package me.potato.sample.msa.authservice.controller.token;

import lombok.extern.slf4j.Slf4j;
import me.potato.sample.msa.authservice.controller.token.dto.TokenRequest;
import me.potato.sample.msa.authservice.controller.token.dto.TokenResponse;
import me.potato.sample.msa.authservice.security.JwtProvider;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/token")
public class TokenController {

    private JwtProvider jwtProvider;

    public TokenController(JwtProvider jwtProvider) {
        this.jwtProvider = jwtProvider;
    }

    @PostMapping
    public ResponseEntity generate(@RequestBody @Valid TokenRequest tokenRequest) {

        String generatedToken = jwtProvider.generate(tokenRequest.getUsername(), tokenRequest.getPassword());
        return ResponseEntity.ok().body(new TokenResponse(generatedToken));
    }

    @GetMapping
    public String validate(){
        return "hello";
    }


}
