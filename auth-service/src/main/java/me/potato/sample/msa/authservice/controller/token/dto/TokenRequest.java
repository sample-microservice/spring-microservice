package me.potato.sample.msa.authservice.controller.token.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@ToString
public class TokenRequest {

    @NotEmpty
    private String username;

    @NotEmpty
    private String password;

}
