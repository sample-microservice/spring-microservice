package me.potato.sample.msa.authservice.security;

import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import me.potato.sample.msa.authservice.model.JwtUserPrincipal;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Slf4j
@Component
public class JwtProvider {

    @Value("${jwt.secrets}")
    private String jwtSecrets;

    @Value("${jwt.expiration}")
    private int jwtExpiration;

    private final AuthenticationManager authenticationManager;

    public JwtProvider(AuthenticationManager authManager) {
        this.authenticationManager = authManager;
    }

    public String generate(String username, String password) {
        if (authenticationManager == null)
            return "";

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        username,
                        password
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
        JwtUserPrincipal details = (JwtUserPrincipal) authentication.getPrincipal();

        Date issuedTime = Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant());
        Date expiredTime = Date.from(issuedTime.toInstant().plusSeconds(jwtExpiration));

        return Jwts.builder()
                .setSubject(details.getUsername())
                .setIssuedAt(issuedTime)
                .setExpiration(expiredTime)
                .signWith(SignatureAlgorithm.HS256, jwtSecrets)
                .compact();
    }

    public String getUsernameFromToken(String token) {

        return Jwts.parser()
                .setSigningKey(jwtSecrets)
                .parseClaimsJws(token)
                .getBody().getSubject();
    }

    public boolean validateJwt(String jwt) {
        try{
            Jwts.parser().setSigningKey(jwtSecrets).parseClaimsJws(jwt);
            return true;
        }catch (SignatureException e){
            log.error("Invalid JWT signature -> Message {}", e.getMessage());
        }catch (MalformedJwtException e){
            log.error("Invalid JWT token -> Message {}", e.getMessage());
        }catch (ExpiredJwtException e){
            log.error("Expired JWT token -> Message {}", e.getMessage());
        }catch (UnsupportedJwtException e){
            log.error("Unsupported JWT token -> Message {}", e.getMessage());
        }catch (IllegalArgumentException e){
            log.error("Jwt Claims is empty -> Message {}", e.getMessage());
        }

        return false;
    }
}
