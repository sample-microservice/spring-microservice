package me.potato.sample.msa.authservice.service;

import lombok.extern.slf4j.Slf4j;
import me.potato.sample.msa.authservice.controller.account.dto.SignUpRequest;
import me.potato.sample.msa.authservice.model.account.Account;
import me.potato.sample.msa.authservice.model.account.AccountRepository;
import me.potato.sample.msa.authservice.model.role.Roles;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
public class AccountService {

    private final AccountRepository accounts;
    private final PasswordEncoder encoder;


    public AccountService(AccountRepository accounts, PasswordEncoder encoder) {
        this.accounts = accounts;
        this.encoder = encoder;
    }

    public Account createNewAccount(SignUpRequest signUpRequest) {

        Account newAccount = Account.builder()
                .username(signUpRequest.getUsername())
                .password(encoder.encode(signUpRequest.getPassword()))
                .expirationDate(signUpRequest.getExpirationDate())
                .build();

        List<Roles> roles = signUpRequest.getRoles().stream().map(stringRole -> Roles.valueOf(stringRole)).collect(Collectors.toList());
        newAccount.setRoles(roles);

        Account save = accounts.saveAndFlush(newAccount);
        log.info("Account {} Created", newAccount);
        return save;
    }

    public boolean existAccountByUsername(String username){
        return accounts.existsByUsername(username);
    }

    public Account getAccountbyUsername(String username){
        return accounts.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username));
    }

}
