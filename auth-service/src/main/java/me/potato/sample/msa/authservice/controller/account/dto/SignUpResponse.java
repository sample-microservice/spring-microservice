package me.potato.sample.msa.authservice.controller.account.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;
import java.util.Set;

@Getter
@Setter
@Builder
public class SignUpResponse {

    @NotEmpty
    private Long id;

    @NotEmpty
    private String username;

    @NotEmpty
    private Set<String> roles;

    @NotEmpty
    private LocalDateTime expirationDate;

    @NotEmpty
    private LocalDateTime created;
}
