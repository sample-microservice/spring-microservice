package me.potato.sample.msa.authservice.controller.account;

import me.potato.sample.msa.authservice.controller.account.dto.SignUpRequest;
import me.potato.sample.msa.authservice.controller.account.dto.SignUpResponse;
import me.potato.sample.msa.authservice.controller.exception.ApiError;
import me.potato.sample.msa.authservice.model.account.Account;
import me.potato.sample.msa.authservice.service.AccountService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("/accounts")
public class AccountConroller {

    private final AccountService accountService;

    public AccountConroller(AccountService accountService) {
        this.accountService = accountService;
    }


    @PostMapping
    public ResponseEntity create(@RequestBody @Valid SignUpRequest signUpRequest) throws Exception {

        if (accountService.existAccountByUsername(signUpRequest.getUsername()))
            return ResponseEntity.status(CONFLICT).body(new ApiError(CONFLICT, "Username already exists", signUpRequest.getUsername()));

        Account account = accountService.createNewAccount(signUpRequest);

        return ResponseEntity.status(CREATED).body(
                SignUpResponse.builder()
                        .id(account.getId())
                        .username(account.getUsername())
                        .roles(account.getRoles().stream().map(role -> role.name()).collect(Collectors.toSet()))
                        .expirationDate(account.getExpirationDate())
                        .created(account.getCreated())
                        .build()
        );


    }


    @GetMapping
    public Account getAccountInfo(@NotEmpty @RequestParam String username) {
        return accountService.getAccountbyUsername(username);
    }

}
