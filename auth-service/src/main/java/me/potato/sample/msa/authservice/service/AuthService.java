package me.potato.sample.msa.authservice.service;

import me.potato.sample.msa.authservice.security.JwtProvider;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.stereotype.Service;

@Service
public class AuthService {

    private final AuthenticationManager authenticationManager;

    private final JwtProvider jwtProvider;


    public AuthService(AuthenticationManager authenticationManager, JwtProvider jwtProvider) {
        this.authenticationManager = authenticationManager;
        this.jwtProvider = jwtProvider;
    }

//
//    public Authentication signin(String username, String password){
//
//        // 인증 실패 시 Spring Security 내부에서 AuthenticationException이 발생됨,
//        // 인증 실패 사유로는 패스워드 불일치, 계정 기간 만료등으로 다양함, 따라서 UserDetailsService.loadUserByUsername에서는
//        // spring security에서는 password가 불일치 하더라도 DB에서 정보를 읽어 와서 처리함
//
//        Authentication authenticate = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
//                username,
//                password
//        ));
//
//        SecurityContextHolder.getContext().setAuthentication(authenticate);
//
//        jwtGenerator.generate(authenticate);
//    }
}
