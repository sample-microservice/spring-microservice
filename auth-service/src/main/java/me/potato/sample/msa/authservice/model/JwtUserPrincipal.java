package me.potato.sample.msa.authservice.model;

import lombok.extern.slf4j.Slf4j;
import me.potato.sample.msa.authservice.model.account.Account;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;


@Slf4j
public class JwtUserPrincipal implements UserDetails {

    private final Account account;

    private List<GrantedAuthority> authorities;

    public JwtUserPrincipal(Account account) {
        this.account = account;
        setAuthorities();

    }

    private void setAuthorities() {

        log.info("roles = size {}",account.getRoles());

        List<GrantedAuthority> collect = account.getRoles().stream().map(
                aRole -> new SimpleGrantedAuthority(aRole.toString())
        ).collect(Collectors.toList());

        authorities = collect;
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {


        return null;
    }

    @Override
    public String getPassword() {
        return account.getPassword();
    }

    @Override
    public String getUsername() {
        return account.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        // DB에 저장된 만료일 점검
        return  !account.getExpirationDate().isBefore(LocalDateTime.now());
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
