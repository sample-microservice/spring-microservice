package me.potato.sample.msa.authservice.service;


import me.potato.sample.msa.authservice.model.JwtUserPrincipal;
import me.potato.sample.msa.authservice.model.account.Account;
import me.potato.sample.msa.authservice.model.account.AccountRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    public final AccountRepository accountRepository;

    public UserDetailsServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public JwtUserPrincipal loadUserByUsername(String username) throws UsernameNotFoundException {

        Account fromPersistence = accountRepository
                .findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException(username));


        // todo: 이거 factory로 바꿔야 할듯
        return new JwtUserPrincipal(fromPersistence);
    }

}
