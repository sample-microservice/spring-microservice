package me.potato.sample.msa.authservice.model.role;

public enum Roles {
    ROLE_USER,
    ROLE_ADMIN,
    ROLE_MANAGEMENT;
}
